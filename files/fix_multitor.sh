#!/usr/bin/env bash
/usr/local/bin/multitor --show-id --socks-port 19000
if [ $? -eq 0 ]; then
    echo OK
else
echo FAIL
/usr/local/bin/multitor -k
source /root/scripts/start_multitor.sh
#/usr/local/bin/multitor --init 100 --user root --socks-port 19000 --control-port 29900 --proxy socks
fi