#!/usr/bin/env bash
COUNT=40
if test -f "/multitorcount"; then
    COUNT=`cat /multitorcount`
fi

/usr/local/bin/multitor --init $COUNT --user toranon --socks-port 19000 --control-port 19900 --proxy socks